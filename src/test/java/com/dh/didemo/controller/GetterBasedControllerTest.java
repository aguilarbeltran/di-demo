package com.dh.didemo.controller;

import com.dh.didemo.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetterBasedControllerTest {
    private GetterBasedController getterBasedController;

    @Before
    public void befero() throws Exception {
        getterBasedController = new GetterBasedController();
        getterBasedController.setGreetingService(new GreetingServiceImpl());
    }

    @Test
    public void sayhello() {
        String greeting = getterBasedController.sayhello();
        Assert.assertEquals(GreetingServiceImpl.GREENTING, greeting);
    }
}