package com.dh.didemo;

import com.dh.didemo.controller.MyController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
@SpringBootApplication
@ImportResource("classpath:beanconfig.xml")
@ComponentScan(basePackages = {"com.dh.didemo", "otro"})
public class DiDemoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DiDemoApplication.class, args);
        MyController controller = (MyController) context.getBean("myController");
        controller.hello();

       /* System.out.println(context.getBean(PropertyBasedController.class).sayhello());
        System.out.println(context.getBean(GetterBasedController.class).sayhello());
        System.out.println(context.getBean(ConstructorBasedController.class).sayhello());
        System.out.println(context.getBean(Forecast.class).weather());
        System.out.println(context.getBean(FakeJMS.class).getUser());
        System.out.println(context.getBean(FakeJMS.class).getPassword());*/
        System.out.println((context.getBean(FakeDataSourse.class).getUser()) + " " + "clase fakedatasourse");
        //System.out.println(context.getBean(FakeDataSourse.class).getEnv());


        System.out.println((context.getBean(FakeJMS.class).getUser()) + "" + "clase fakejsm");
        // System.out.println(context.getBean(FakeJMS.class).getEnv());


    }
}