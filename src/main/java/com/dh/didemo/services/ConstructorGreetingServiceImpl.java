package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class ConstructorGreetingServiceImpl implements GreetingService {

    public static final String GREENTING = "hello ConstructorGreetingServiceImpl";

    @Override
    public String sayGreeting() {
        return GREENTING;
    }
}
