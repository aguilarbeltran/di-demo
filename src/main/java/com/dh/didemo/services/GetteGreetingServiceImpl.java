package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class GetteGreetingServiceImpl implements GreetingService {

    public static final String GREENTING = "hello GetteGreetingServiceImpl";

    @Override
    public String sayGreeting() {
        return GREENTING;
    }
}
